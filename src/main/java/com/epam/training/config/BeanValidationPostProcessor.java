package com.epam.training.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;

public class BeanValidationPostProcessor implements BeanPostProcessor {
    private static final Logger logger =
            LogManager.getLogger(BeanValidationPostProcessor.class.getName());

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof Validator) {

            final DataBinder dataBinder = new DataBinder(bean);
            dataBinder.addValidators((Validator) bean);
            dataBinder.validate();
            if (dataBinder.getBindingResult().hasErrors()) {
                logger.error(bean);
                logger.error(dataBinder.getBindingResult().getAllErrors().get(0).toString());
            } else {
                logger.info(bean + " validated");
            }
        }
        return bean;
    }
}
