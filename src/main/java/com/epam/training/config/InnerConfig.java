package com.epam.training.config;

import com.epam.training.model.*;
import org.springframework.context.annotation.*;

@Configuration
public class InnerConfig {
    @Bean(value = "beansValidator")
    public static BeanValidationPostProcessor getBeanValidationPostProcessor() {
        return new BeanValidationPostProcessor();
    }

    @Bean(value = "beanBFactoryPostProcessor", initMethod = "init")
    @DependsOn(value = "beansValidator")
    public static BeanBFactoryPostProcessor getConfigBeanB() {
        return new BeanBFactoryPostProcessor();
    }

    @Bean("beanA")
    @DependsOn(value = "beanC")
    public BeanA getBeanA() {
        return new BeanA();
    }

    @Bean(value = "beanB", initMethod = "init", destroyMethod = "destroy")
    @DependsOn(value = "beanD")
    public BeanB getBeanB() {
        return new BeanB();
    }

    @Bean(value = "beanC", initMethod = "init", destroyMethod = "destroy")
    @DependsOn(value = "beanB")
    public BeanC getBeanC() {
        return new BeanC();
    }

    @Bean(value = "beanD", initMethod = "init", destroyMethod = "destroy")
    @DependsOn(value = "beansValidator")
    public BeanD getBeanD() {
        return new BeanD();
    }

    @Bean("beanE")
    @DependsOn(value = "beanC")
    public BeanE getBeanE() {
        return new BeanE();
    }

    @Bean("beanF")
    @Lazy
    @DependsOn(value = "beanC")
    public BeanF getBeanF() {
        return new BeanF();
    }
}
