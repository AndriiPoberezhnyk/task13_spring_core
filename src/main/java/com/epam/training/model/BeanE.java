package com.epam.training.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.Serializable;

public class BeanE implements Serializable, Validator {
    private static final Logger logger =
            LogManager.getLogger(BeanE.class.getName());
    private String name;
    private int value;

    public BeanE() {
        name = "default";
        value = Integer.MAX_VALUE;
    }

    public BeanE(BeanA beanA) {
        name = beanA.getName();
        value = beanA.getValue();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return BeanE.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "name.empty");
        ValidationUtils.rejectIfEmpty(errors, "value", "value.empty");
        BeanE beanE = (BeanE) o;
        if (beanE.getValue() < 0) {
            errors.rejectValue("value", "value.negative");
        }
    }

    @PostConstruct
    public void postConstruct() {
        logger.info("BeanE created. Post construct message");
    }

    @PreDestroy
    public void preDestroy() {
        logger.info("BeanE soon will be destroyed. Pre destroy message");
    }
}