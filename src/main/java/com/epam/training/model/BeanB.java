package com.epam.training.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import java.io.Serializable;

@PropertySource("beans.properties")
public class BeanB implements Serializable, Validator {
    private static final Logger logger =
            LogManager.getLogger(BeanB.class.getName());
    @Value("${BeanB.name}")
    private String name;
    @Value("${BeanB.value}")
    private int value;

    public BeanB() {
    }

    private void init() {
        logger.info("BeanB initialized " + hashCode());
    }

    private void notDefaultInit() {
        logger.info("BeanB initialized by another init method " + hashCode());
    }

    private void destroy() {
        logger.info("BeanB destroyed");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return BeanB.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "name.empty");
        ValidationUtils.rejectIfEmpty(errors, "value", "value.empty");
        BeanB beanB = (BeanB) o;
        if (beanB.getValue() < 0) {
            errors.rejectValue("value", "value.negative");
        }
    }
}
