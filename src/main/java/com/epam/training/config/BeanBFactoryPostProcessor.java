package com.epam.training.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.*;

import java.util.HashMap;
import java.util.Map;

public class BeanBFactoryPostProcessor implements BeanFactoryPostProcessor {
    private Map<String, BeanDefinition> beanConfigurations = new HashMap<>();

    private static final Logger logger =
            LogManager.getLogger(BeanBFactoryPostProcessor.class.getName());

    private void init() {
        logger.info("BeanB BeanFactory init()");
    }

    @Override
    public void postProcessBeanFactory(
            ConfigurableListableBeanFactory beanFactory)
            throws BeansException {
        BeanDefinition beanB = beanFactory.getBeanDefinition("beanB");
        beanB.setInitMethodName("notDefaultInit");
        for (String name: beanFactory.getBeanDefinitionNames()) {
            beanConfigurations.put(name,
                    beanFactory.getBeanDefinition(name));
        }
    }

    public Map<String, BeanDefinition> getBeanConfigurations(){
        return beanConfigurations;
    }
}

