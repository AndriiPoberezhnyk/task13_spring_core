package com.epam.training.config;

import com.epam.training.model.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource("beans.properties")
@Import(InnerConfig.class)
public class OuterConfig {
    @Value("${BeanB.name}")
    private String nameBeanB;
    @Value("${BeanC.name}")
    private String nameBeanC;
    @Value("${BeanC.value}")
    private int valueBeanC;
    @Value("${BeanD.value}")
    private int valueBeanD;

    @Bean("beanA1")
    @DependsOn(value = {"beanC", "beansValidator"})
    public BeanA getBeanA1() {
        return new BeanA(nameBeanB, valueBeanC);
    }

    @Bean("beanA2")
    @DependsOn(value = "beanC")
    public BeanA getBeanA2() {
        return new BeanA(nameBeanB, valueBeanD);
    }

    @Bean("beanA3")
    @DependsOn(value = "beanC")
    public BeanA getBeanA3() {
        return new BeanA(nameBeanC, valueBeanD);
    }

    @Bean("beanE1")
    @DependsOn(value = "beanC")
    public BeanE getBeanE1() {
        return new BeanE(getBeanA1());
    }

    @Bean("beanE2")
    @DependsOn(value = "beanC")
    public BeanE getBeanE2() {
        return new BeanE(getBeanA2());
    }

    @Bean("beanE3")
    @DependsOn(value = "beanC")
    public BeanE getBeanE3() {
        return new BeanE(getBeanA3());
    }
}
