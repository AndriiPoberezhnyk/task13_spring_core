package com.epam.training.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import java.io.Serializable;

@PropertySource("beans.properties")
public class BeanC implements Serializable, Validator {
    private static final Logger logger =
            LogManager.getLogger(BeanC.class.getName());
    @Value("${BeanC.name}")
    private String name;
    @Value("${BeanC.value}")
    private int value;

    public BeanC() {
    }

    private void init() {
        logger.info("BeanC initialized");
    }

    private void destroy() {
        logger.info("BeanC destroyed");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return BeanC.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "name.empty");
        ValidationUtils.rejectIfEmpty(errors, "value", "value.empty");
        BeanC beanC = (BeanC) o;
        if (beanC.getValue() < 0) {
            errors.rejectValue("value", "value.negative");
        }
    }
}
