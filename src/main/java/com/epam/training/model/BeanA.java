package com.epam.training.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import java.io.Serializable;

public class BeanA implements Serializable, Validator, InitializingBean, DisposableBean {
    private static final Logger logger =
            LogManager.getLogger(BeanA.class.getName());
    private String name;
    private int value;

    public BeanA() {
        name = "default";
        value = Integer.MAX_VALUE;
    }

    public BeanA(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return BeanA.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "name.empty");
        ValidationUtils.rejectIfEmpty(errors, "value", "value.empty");
        BeanA beanA = (BeanA) o;
        if (beanA.getValue() < 0) {
            errors.rejectValue("value", "value.negative");
        }
    }

    @Override
    public void destroy() throws Exception {
        logger.info("BeanA is about to be destroyed");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("Init method after properties are set,"
                + " properties ->" + toString() + ", " + hashCode());
    }
}
