package com.epam.training.view;

import com.epam.training.config.BeanBFactoryPostProcessor;
import com.epam.training.config.OuterConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.*;

public class View {
    private static final Logger logger = LogManager.getLogger(View.class.getName());
    private Map<String, String> menu;
    private Map<String, Runnable> methodsMenu;
    private Scanner scanner;
    private ResourceBundle bundle;
    private ApplicationContext context;

    public View() {
        bundle = ResourceBundle.getBundle("Menu");
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        context = new AnnotationConfigApplicationContext(OuterConfig.class);
    }

    public void run() {
        String keyMenu;
        do {
            generateMainMenu();
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception ignored) {
                logger.error(ignored.getStackTrace());
            }
        } while (!keyMenu.equals("Q"));
    }

    private void generateMainMenu() {
        menu.clear();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("Q", bundle.getString("Q"));

        methodsMenu.clear();
        methodsMenu.put("1", this::showBeansName);
        methodsMenu.put("2", this::showBeansDefinition);
        outputMenu();
    }

    private void showBeansName() {
        Arrays.stream(context.getBeanDefinitionNames()).forEach(beanName ->
                logger.trace("Bean[] " + beanName));
    }


    private void showBeansDefinition() {
        BeanBFactoryPostProcessor beanFactory
                = (BeanBFactoryPostProcessor) context
                .getBean("beanBFactoryPostProcessor");
        beanFactory.getBeanConfigurations().forEach((k, v) -> {
            logger.trace("Bean name -> " + k + " Definition ->" + v);
        });
    }

    private void outputMenu() {
        logger.trace("MENU:");
        for (String option : menu.values()) {
            logger.trace(option);
        }
        logger.trace(">>> ");
    }

}
